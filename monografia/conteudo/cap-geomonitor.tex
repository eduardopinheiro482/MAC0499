%% ------------------------------------------------------------------------- %%
\chapter{Uma aplicação para visualização espacial dos dados da saúde}
\label{cap:geomonitor}

Este capítulo descreve uma aplicação \footnote{\url{http://interscity.org/apps/saude}} desenvolvida para visualizar dados da saúde utilizando a \textit{API} oferecida pelo serviço descrito no capítulo anterior como base para suas análises.

\section{GeoMonitor da Saúde}

A Secretaria Municipal de Saúde recebe uma grande quantidade de dados que descrevem as diversas atuações do Sistema Único de Saúde (SUS) em relação à sociedade. Nesses dados estão informações cruciais para a melhoria na qualidade do sistema de saúde pública da cidade. Para a análise de grandes volumes de dados, necessitamos de ferramentas sofisticadas, uma vez que a mera observação dos dados não atende a esse objetivo. Dentro desse contexto, a aplicação \emph{GeoMonitor da Saúde} foi criada com o objetivo de extrair dos dados informações ``sutis'' que uma análise crua dos dados não seria capaz de identificar. 

A plataforma apresenta os dados georreferenciados das internações custeadas pelo SUS. A ferramenta também apresenta informações demográficas, oriundas do Censo de 2010 realizado pelo IBGE e das projeções populacionais disponibilizadas pela Fundação SEADE\footnote{Fundação vinculada à Secretaria de Planejamento e Gestão do Estado de São Paulo, é hoje um centro de referência nacional na produção e disseminação de análises e estatísticas socioeconômicas e demográficas: \url{http://seade.gov.br}.},
 estratificadas por sexo, faixa etária e raça/cor segundo as unidades territoriais administrativas da Prefeitura de São Paulo e da Secretaria Municipal da Saúde. Com o GeoMonitor, é possível realizar buscas de internações hospitalares a partir de certos filtros (estabelecimento de ocorrência, subprefeitura, raça/cor, faixa etária, distância, etc.) e observar o mapeamento do resultado ao longo de São Paulo. Além disso, o sistema tem a capacidade de, a partir dos dados georreferenciados, criar clusters e mapas de calor que são apresentados no mapa da cidade.

\section{Dados utilizados}

Os dados utilizados foram cedidos pela Secretaria Municipal da Saúde de São Paulo a partir das bases públicas referentes aos Sistemas de Informação de mortalidade (SIM), de Nascidos Vivos (SINASC) e Hospitalar (SIH). Até a presente data, no contexto desta monografia, a SMS-SP nos forneceu a base de dados SIH. Todos os dados fornecidos à equipe do IME-USP foram anonimizados, i.e., não houve informações pessoais sobre pacientes específicos e o domicílio do paciente não foi fornecido na forma de endereço, mas apenas com a identificação do setor censitário correspondente \footnote{Processo que no futuro poderá ser efetuado pelo módulo de Geoanonimização descrito na Seção \ref{sec:anom}}.

Esse primeiro conjunto de dados do SIH, fornecido pela SMS-SP, foi dividido entre dados dos estabelecimentos e dados das internações. Dos estabelecimentos de saúde, teremos as informações como CNES, latitude e longitude, quantidade de leitos, telefone, classificação, distrito administrativo, subprefeitura, supervisão técnica de saúde e coordenadoria regional de saúde a qual pertence. Das internações hospitalares teremos dados sobre o setor censitário do paciente, estabelecimento de saúde, especialidade, CID, data, distância viária entre setor censitário do paciente e o estabelecimento de saúde e informações do paciente (Idade, sexo, nível de instrução e grupo étnico).

\section{Aplicação}

Para o desenvolvimento da aplicação, foram adotadas as boas práticas de desenvolvimento de software livre que já vem sendo utilizadas pelo grupo de sistemas do IME-USP nos últimos 20 anos. Uma dessas técnicas, o método ágil \footnote{\url{http://www.manifestoagil.com.br/}} de desenvolvimento, foi aplicado da seguinte forma: Iterações de curta duração, normalmente duas semanas, eram planejadas e executadas pelos desenvolvedores. Seguidas de homologação por parte da SMS-SP que verificava o trabalho executado e criava novas demandas. Elaborando com isso, uma nova iteração.

O repositório \url{https://gitlab.com/interscity/health-dashboard/health-smart-city}, licenciado sob a Mozilla Public License 2.0 \footnote{\url{https://www.mozilla.org/en-US/MPL/2.0/}}, foi criado para o desenvolvimento da aplicação, e sua organização em torno do código-fonte desenvolvido. Nele, está documentado todo o trabalho desenvolvido e todo o trabalho futuro para a evolução da aplicação, assim como reuniões com representantes da SMS-SP que acompanharam o desenvolvimento.

\subsection{Home}
Na página inicial (vide Figura \ref{fig:home}), temos algumas informações sobre os dados presentes na aplicação e uma lista de atalhos para os serviços disponíveis.

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{Home}
	\end{center}
	\captionof{figure}{Página Home}
	\label{fig:home}
\end{figure}
\subsection{Dados Gerais}
Na página de Dados Gerais (vide Figura \ref{fig:dados}), são apresentados ao usuário análises feitas sobre os dados. Dentre elas estão:

\bigskip
\begin{itemize}
	\item Ranking dos estabelecimentos por internações efetuadas
	\item Gráfico indicando a quantidade de internações por mês
	\item Gráfico mostrando a distância viária entre o paciente e o estabelecimento de saúde utilizado por especialidade.
	\item Gráfico indicando a porcentagem e valor absoluto de internações por especialidade.
	\item Seção onde o usuário pode escolher uma variável, e a partir dela é gerado um histograma de seus valores.
\end{itemize}

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{dados}
	\end{center}
	\captionof{figure}{Página de Dados Gerais}
	\label{fig:dados}
\end{figure}

\subsection{Busca avançada}
Na página de busca avançada (vide Figura \ref{fig:busca}), o usuário pode filtrar os dados a partir dos campos da base de dados. Por exemplo, se o usuário quer apenas as internações em que a especialidade do leito seja ``Cuidados Prolongados'', ele pode fazer tal filtro e observar o resultado no mapa onde as internações, que se enquadram na sua busca, serão identificadas.

A partir dos dados filtrados, a plataforma gera duas visualizações distintas: a primeira clusterizando os dados de acordo com sua posição no mapa e a segunda gerando um mapa de calor de acordo com a concentração do pontos no mapa. A partir dessas duas visualizações, é possível identificar regiões da cidade onde um grande número de pessoas utilizaram o SUS pelo motivo especificado nos filtros. Identificando assim, necessidades de recursos hospitalares que não estão sendo atendidas com base nos dados analisados. O usuário pode escolher o raio de convergência a ser utilizado pelos algoritmos que irão gerar as visualizações, podendo assim fazer tanto análises locais como análises mais abrangentes.

Também nessa página, o usuário tem a opção de visualizar no mapa os seguintes limites administrativos: 

\bigskip
\begin{itemize}
	\item Município de São Paulo
	\item Coordenadoria Regional de Saúde
	\item Supervisão Técnica de Saúde
	\item Prefeitura Regional
	\item Distritos Administrativos
	\item Áreas de Abrangência de UBS
	\item Áreas de Cobertura da Estratégia Saúde da Família
\end{itemize}
\bigskip

Com isso um gestor público de Saúde, que atua em alguma dessas áreas, pode visualizar as internações que ocorreram dentro de seu limite de atuação, facilitando o seu processo de tomada de decisão.

Por fim, o usuário tem ainda as seguintes opções de utilização dos dados:

\bigskip
\begin{itemize}
	\item Download dos dados de acordo com os filtros aplicados
	\item Gerar a página de Dados Gerais apenas com os dados filtrados
	\item Imprimir o mapa que está sendo visualizado.
\end{itemize}
\bigskip

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{busca}
	\end{center}
	\captionof{figure}{Página de Busca Avançada}
	\label{fig:busca}
\end{figure}

\subsection{Estabelecimentos}

Na página de estabelecimentos (vide Figura \ref{fig:estabelecimentos}), é apresentado ao usuário todos os estabelecimentos de saúde da cidade de São Paulo que efetuaram internações hospitalares financiadas pelo SUS. Ao selecionar um deles, o usuário tem informações sobre o estabelecimento, bem como a localização no mapa do setor censitário dos pacientes que utilizaram tal estabelecimento. Assim como na página de ``Busca Avançada'', são apresentados clusters das internações hospitalares e mapa de calor de acordo com a concentração delas.

Para facilitar a visualização, a página também mostra os raios de concentração das internações hospitalares: por exemplo, o primeiro raio indica a região de residência de 25\% dos pacientes que se deslocaram até o estabelecimento escolhido; o segundo raio 50\% e; o terceiro raio 75\%; dessa forma, facilitando a identificação de estabelecimentos em que pacientes necessitam se locomover por maiores distâncias para serem atendidos. 

\smallskip

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{Estabelecimentos}
	\end{center}
	\captionof{figure}{Página de Estabelecimentos}
	\label{fig:estabelecimentos}
\end{figure}

\subsection{Sobre}

Na página Sobre (vide Figura \ref{fig:estabelecimentos}), é apresentada uma visão geral do projeto, informações sobre o desenvolvimento, dados utilizados e uma pequena apresentação do sistema e dos membros do projeto.

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{sobre}
	\end{center}
	\captionof{figure}{Págine Sobre}
	\label{fig:sobre}
\end{figure}

\subsection{Perguntas Frequentes}

Na página de Perguntas Frequentes (vide Figura \ref{fig:faq}), estão esclarecidas dúvidas recorrentes sobre a utilização da aplicação.

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{faq}
	\end{center}
	\captionof{figure}{Página de Perguntas Frequentes}
	\label{fig:faq}
\end{figure}

\section{Detalhes de implementação}

Para implementação do GeoMonitor, selecionamos uma série de ferramentas e bibliotecas, sob licença de \emph{Software} Livre, que permitiu o desenvolvimento de uma aplicação de visualização de dados georreferenciados da saúde, de acordo com o que estava disponível no estado-da-prática das tecnologias envolvidas, conforme apresentadas a seguir.

\subsection{Open Street Maps}

Seguindo os princípios que guiaram o desenvolvimento deste projeto, utilizamos a \textit{API} do Open Street Maps \footnote{\url{http://openstreetmap.org}} para criar suas visualizações. Essa \textit{API} é licenciada como \emph{Software} Livre, desenvolvida por uma grande comunidade. Utilizamos a biblioteca Leaflet \footnote{\url{http://leafletjs.com}}, também \emph{Software} Livre, para gerir os elementos que são apresentados no mapa.

\subsection{Cluster}

Com um volume muito grande de dados que são recebidos, torna-se necessário uma forma inteligente de apresentá-los. A clusterização é um método muito comum para lidar com esse tipo de problema, de forma que, os dados geolocalizados tornam a visualização dos clusters extremamente intuitiva e informativa. Geramos os clusters utilizando a biblioteca Leaflet.markercluster \footnote{\url{https://github.com/Leaflet/Leaflet.markercluster}}.

\subsection{Mapa de Calor}

Outra forma muito comum de visualização de dados são mapas de calor, que facilitam a identificação de áreas com grande concentração de dados. Utilizamos esse método para apresentar ao usuário áreas no mapa de grande concentração de internações. Os mapas de calor são gerados com a biblioteca Heatmap.js \footnote{\url{http://patrick-wied.at/static/heatmapjs/?utm_source=gh}}.

\subsection{Gráficos}

Para criação de gráficos dinâmicos e adaptativos que podem ter seu conteúdo alterado de acordo com a necessidade do usuário, utilizamos a biblioteca echarts \footnote{\url{http://echarts.baidu.com}}.

\subsection{Distância Percorrida}

Inicialmente, utilizamos a Distância Euclidiana para realizar as análises da plataforma, porém para aproximar essas análises à realidade, a plataforma foi alterada para calcular a distância viária percorrida pelo paciente, ou seja, a distância de acordo com as vias públicas disponíveis. Isso foi alcançado com a ajuda do \emph{software Open Source Routing Machine (OSRM)} \footnote{\url{http://project-osrm.org/}}, uma aplicação \emph{Software} Livre que, utilizando dados viários públicos, gera rotas de deslocamento similares às encontradas em sistemas proprietários, como o Google Maps \footnote{\url{https://google.com/maps}}. O OSRM possui uma API que aceita requisições com os pontos geográficos de origem e destino respondendo com as informações relevantes (distância, rota, número de ruas, etc).

Utilizando uma instância própria do \emph{software}, geramos primeiramente todos os pares de origem e destino que a base de dados possuía. Como os dados estão geoanonimizados, obtivemos o valor de 166.600 pares distintos, via um \emph{script} para realizar os pedidos e guardar as respostas. Com essa informação, todos os registros da base de dados receberam a distância percorrida de acordo com seus dados.

\pagebreak
\section{Exemplos de utilização}
A partir do \textbf{GeoMonitor da Saúde} podemos encontrar padrões que não seriam visíveis sem uma ferramenta de visualização de dados.
%
Alguns exemplos:
\begin{itemize}
	\item Concentração de casos de Leucemia Línfoide na região leste da cidade de São Paulo.
	\item Casos de HIV se concentram no centro da cidade.
\end{itemize}

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{leucemia}
		\caption{Internações relacionadas a casos de Leucemia Linfóide}
		\label{fig:leucemia}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.48\textwidth}
		\includegraphics[width=\textwidth]{hiv}
		\caption{Internações relacionadas a casos de HIV}
		\label{fig:hiv}
	\end{minipage}
\end{figure}

A aplicação oferece 25 filtros distintos ao usuário. Tornando possível criar uma grande quantidade de combinações de busca que podem ser usadas para encontrar anomalias nos dados. E assim, fazer um uso mais inteligente dos recursos de saúde.
