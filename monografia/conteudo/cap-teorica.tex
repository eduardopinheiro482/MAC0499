%% ------------------------------------------------------------------------- %%
\chapter{Clusterização de dados}
\label{cap:teorica}

Análise de dados é parte fundamental da Ciência da Computação. Com o recente avanço na capacidade de armazenamento de dados e na ascensão de tecnologias de Big Data, manipular dados com o objetivo de extrair informações se tornou uma necessidade~\citep{HuWen2014}. Dentro dessa realidade, clusterização de dados se tornou uma prática muito usada, como método de simplificar dados perdendo o mínimo possível de informação.

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{before_cluster}
		\caption{Dados não clusterizados}
		\label{fig:before_cluster}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{after_cluster}
		\caption{Dados clusterizados}
		\label{fig:after_cluster}
	\end{minipage}
\end{figure}

Clusterização é a classificação não supervisionada dos dados através de seus padrões em grupos onde essas características são semelhantes \citep{Jain:1999:DCR:331499.331504} baseando-se em alguma métrica de distância estabelecida, com o objetivo de minimizar a distância entre objetos no mesmo cluster e maximizar a distância entre os diferentes clusters \citep{Amatriain2011}, com essa classificação podemos abstrair informações que não foram caracterizadas como relevantes e realçar as características que o cluster possui. Como ilustração, as Figuras \ref{fig:before_cluster} e \ref{fig:after_cluster} representam dados de internações hospitalares por aborto na cidade de São Paulo. Na Figura \ref{fig:after_cluster} vemos facilmente a concentração dos casos na zona Leste da cidade.

\section{Componentes de uma tarefa de Clusterização}
\label{sec:cluster}

\citet{Jain:1999:DCR:331499.331504} descrevem os passos necessários para um algoritmo de clusterização: 

\begin{enumerate}
	\item Representação dos padrões (opcionalmente incluindo extração e/ou seleção de características): faz referência ao número de classes e padrões disponíveis, e também ao número, tipo e escala das características disponíveis ao algoritmo de clusterização, algumas dessas informações podem não ser controláveis por quem utilizará o algoritmo.
	\begin{itemize}
		\item \textbf{Seleção de características:} é o processo de identificar o subgrupo de características mais efitivo 					para o algoritmo de clusterização.
		\item \textbf{Extração de características:} é o uso de uma ou mais transformações dos dados de entrada para produzir 					novas características mais interessantes.
	\end{itemize}

	\item Definição da medida de aproximação apropriada aos dados: normalmente é calculada pela função pré-definida de distância entre os objetos. Uma variedade de funções de distância são usadas de acordo com o domínio dos dados. Um exemplo de função muito usada é a distância euclidiana.

	\item Clusterização ou agrupamento: existem diversos métodos para se realizar a clusterização, conforme a apresentado na Seção~\ref{sec:tiposclusterizacao}.

	\item Abstração dos dados (se necessário): é o processo de extrair uma representação simples e compacta dos dados. Simplicidade em termos de análise automatizada (Uma máquina pode processar os dados), ou em termos de compreensão humana (A representação encontrada é intuitiva e de fácil compreensão).

	\item Avaliação do resultado (se necessário).
\end{enumerate}


\subsection{Tipos de Clusterização}
\label{sec:tiposclusterizacao}

A Figura \ref{fig:clustering} apresenta uma visão geral das técnicas mais utilizadas para clusterização. \citet{Jain:1999:DCR:331499.331504} separa técnicas de clusterização em dois conjuntos distintos \textbf{\textit{Hierarchical}} e \textbf{\textit{Partitional}}. A seguir definimos cada um dos conjuntos, e descrevemos alguns dos principais algoritmos de cada grupo.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=\textwidth]{clustering}
	\end{center}
	\caption{Tipos de Clusterização}
	\label{fig:clustering}
\end{figure}


As técnicas do tipo \textbf{\textit{Hierarchical}} organizam o conjunto de dados em uma estrutura hierárquica de acordo com a proximidade entre os objetos. O resultado de um algoritmo de clusterização hierárquica é geralmente uma árvore binária ou dendrograma \footnote{Árvore que iterativamente divide a base de dados em subconjuntos menores} \citep{Mara2015}.

	\begin{itemize}
		\item \textit{Single Link:} a distância entre dois \textit{clusters} é a menor distância entre pares de objetos.
		\item \textit{Complete Link:} \label{complete-link} a distância entre dois \textit{clusters} é a maior distância entre pares de objetos.
	\end{itemize}

Em ambos, \textit{Single Link} e \textit{Complete Link}, \textit{clusters} são agrupados para formar \textit{clusters} maiores hierarquicamente de acordo com o críterio de distância minima. Nos algoritmos \textit{Complete Link} os \textit{clusters} são compactos e fortemente ligados. Por outro lado, no caso dos algoritmos \textit{Single Link} pode ocorrer um efeito de encadeamento, ou seja, o \textit{cluster} pode ficar longo e "deformado" \citep{Jain:1999:DCR:331499.331504}. Aplicando as técnicas no mesmo conjunto de dados, com dois grupos distintos \{1, 2\} e dados com ruído \{*\}, vemos o resultado nas Figuras \ref{fig:single-link} e \ref{fig:complete-link}.

	\begin{figure}[ht]
		\centering
		\begin{minipage}[b]{0.47\textwidth}
			\includegraphics[width=\textwidth]{single-link}
			\caption{Clusterização Hierárquica \textit{Single Link}}
			\label{fig:single-link}
		\end{minipage}
		\hfill
		\begin{minipage}[b]{0.47\textwidth}
			\includegraphics[width=\textwidth]{complete-link}
			\caption{Clusterização Hierárquica \textit{Complete Link}}
			\label{fig:complete-link}
		\end{minipage}
	\end{figure}

Nas técnicas do tipo \textbf{\textit{Partitional}}, os algoritmos de clusterização dividem a base de dados em k grupos, escolhendo k objetos como sendo centros, os demais objetos são então divididos entre os k grupos de acordo com a medida de aproximação ao centro definida \citep{Mara2015}.

	\begin{itemize}
		\item \textit{Square Error:} o mais intuitivo e mais utilizado entre os algoritmos particionais. A função de distância para a clusterização $\theta$, com conjunto de características $\chi$ (Contendo K clusters) é definida pela função \citep{Jain:1999:DCR:331499.331504}:
		\begin{center}
		$e^2\left ( \chi, \theta   \right ) = \sum_{j = 1}^{K} \sum_{i = 1}^{n_j} \left \| x_i^{(j)} - c_j \right \|^{2}$
		\end{center}
		\item \textit{Graph Theoretic:} são construídas árvores geradoras de custo mínimo (\textit{Minimum spanning tree}) a partir dos dados. Arestas de maior comprimento são removidas para gerar os \textit{clusters} \citep{Jain:1999:DCR:331499.331504}.
	\end{itemize}

\begin{figure}[ht]
	\begin{center}
	\includegraphics[width=0.50\textwidth]{graph-theoretic}
	\end{center}
	\caption{Clusterização \textit{Graph Theoretic}}
	\label{fig:graph-theoretic}
	\end{figure}

A Figura \ref{fig:graph-theoretic} descreve o processo de clusterização \textit{Graph Theoretic} para nove pontos. A aresta CD de maior comprimento, pode ser desfeita criando dois \textit{clusters} \{A, B, C\} e \{D, E, F, G, H, I\}. O processo pode ser feito novamente, quebrando a aresta EF criando dois \textit{clusters} \{D, E\} e \{F, G, H, I\} e assim por diante até um certo comprimento definido.

\section{Clusterização Geoespacial}

Assim como no problema geral de clusterização discutido no Seção \ref{sec:cluster}, no problema de clusterização geoespacial buscamos agrupar objetos semelhantes no mesmo cluster enquanto maximizamos a distância entre \textit{clusters}; nesse caso, o algoritmo toma essa decisão baseado na distância geográfica entre os objetos \citep{doi:10.1080/13658811003702147}.

O algoritmo utilizado no GeoMonitor da Saúde é o algoritmo denominado Clusterização gulosa hierárquica (\textit{Hierarchical greedy clustering})  que está na categoria de algoritmos de clusterização hierárquica \textit{Complete Link} (vide Seção \ref{complete-link}), disponibilizado na biblioteca de clusterização geoespacial ``Leaflet.makercluster'' \footnote{\url{https://github.com/Leaflet/Leaflet.markercluster}}.

Como definido por \cite{clusteringleaflet}, a base do algoritmo de clusterização segue os seguintes passos:

\begin{enumerate}
	\item Começa com um ponto qualquer dos dados.
	\item Encontra todos os pontos dentro de um raio específico.
	\item Forma um cluster com esses pontos.
	\item Encontra um novo ponto que não faz parte de nenhum cluster.
	\item Cria um novo cluster a partir dele.
	\item Repita o processo até visitar todos os pontos.
\end{enumerate}

Como a clusterização é feita em um mapa onde é possível alterar o nível de aproximação (\textit{zoom}), seria necessário executar o algoritmo para todos os níveis de aproximação existentes, o que seria muito custoso em termos de desempenho, para contornar esse problema temos a abordagem hierárquica do algoritmo, reutilizando os cálculos efetuados. 

A Figura \ref{fig:hierarchical} apresenta esse processo. Os valores z18, z17, z16, z15, e z14 na figura representam os diversos níveis de aproximação. Tendo calculado os clusters no nível z18, podemos agrupar os clusters resultantes em clusters no nível z17, da mesma forma encontramos os clusters no nível z16 e assim por diante. Num processo hierárquico podemos criar clusters a partir de clusters, minimizando consideravelmente a quantidade de dados a ser processada em cada nível.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=.8\textwidth]{Hierarchical}
	\end{center}
	\caption{Clusterização hierárquica}
	\label{fig:hierarchical}
\end{figure}

\section{Clusterização no GeoMonitor da Saúde}

No caso do GeoMonitor da Saúde, o processo de anonimização (vide Seção \ref{sec:anom}) tem o efeito de reduzir o problema de clusterização das internações hospitalares (554.202 no ano de 2015), no problema de clusterizar setores censitários \footnote{Setor censitário é a unidade territorial de controle cadastral da coleta do censo do IBGE, constituída por áreas contíguas, respeitando-se os limites da divisão político-administrativa, dos quadros urbano e rural legal e de outras estruturas territoriais de interesse, além dos parâmetros de dimensão mais adequados à operação de coleta.} (18.953 na cidade de São Paulo), pois como vemos no algoritmo descrito por \cite{clusteringleaflet}, pontos com a mesma localização serão agrupados no mesmo cluster. 
Dessa forma, a clusterização do GeoMonior da Saúde é feita em duas etapas:

\begin{enumerate}
	\item Agrupamos os dados de acordo com seu setor censitário, etapa feita pelo módulo de agrupamento, descrito na Seção \ref{mod:agrupamento}.
	\item Utilizamos uma versão modificada do algoritmo de clusterização que atribui a cada ponto a ser clusterizado um peso de acordo com a quantidade de internações que ocorreram no setor censitário representado pelo ponto. O algoritmo padrão assume peso unitário a todos os pontos.
\end{enumerate}

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Cluster_default}
		\caption{Método de Clusterização padrão da biblioteca Leaflet.markercluster}
		\label{fig:Cluster_default}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Cluster_edited}
		\caption{Método de Clusterização Modificado do GeoMonitor da Saúde}
		\label{fig:Cluster_edited}
	\end{minipage}
\end{figure}

Como podemos ver na Figura \ref{fig:Cluster_default}, no algoritmo padrão a clusterização não leva em conta a quantidade de internações que ocorreram em um mesmo ponto, assim somando os valores dos clusters teríamos apenas 16.868 (número de setores censitários em que ocorreram pelo menos uma internação) pontos nos clusters. Já na Figura \ref{fig:Cluster_edited} ao somar os valores temos o resultado correto de 554.202 internações.

No próximo capítulo, apresentamos a proposta de um \textit{serviço} para base de dados geolocalizados, descrevendo uma arquitetura que atuará para fornecer dados a aplicações de visualização. O \textit{serviço} em sua arquitetura, define um módulo de \textit{Agrupamento} dos dados que visa auxiliar algoritmos de clusterização como os vistos neste capítulo e utilizados na aplicação \textit{GeoMonitor da Saúde}.
