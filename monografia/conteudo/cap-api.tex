%% ------------------------------------------------------------------------- %%
\chapter{Proposta de serviço para base de dados geolocalizados da saúde}
\label{cap:api}

A Secretaria municipal de saúde de São Paulo recebe dados de muitas fontes diferentes. Esses dados não são padronizados, dificultando assim a sua utilização por aplicações de visualização de dados, já que sua heterogeneidade implica na necessidade de implementação de aplicações especificas para cada base de dados existente. Isso dificulta também a adoção dessas tecnologias por gestores públicos. 

Com o objetivo de mitigar esse problema, propomos a arquitetura de um serviço para base de dados, inicialmente da saúde, prevendo expansão para dados georreferenciados de qualquer fonte. Essa arquitetura busca abstrair as camadas de manipulação dos dados com o objetivo de oferecer a aplicações uma \textit{API} de fácil utilização para inserção e consulta de dados independente da sua origem. A partir disso, a mesma aplicação poderá apresentar análises de dados da saúde, segurança e mobilidade urbana.

\section{Decisões de \textit{Design}}

Dado o contexto em que a aplicação foi desenvolvida, alguns princípios fundamentais foram estabelecidos: 

\begin{itemize}
	\item \textbf{Software Livre:} utilização de componentes de software existentes é uma pratica fundamental de engenharia de software para aumentar produtividade, eficiência e qualidade de software. No desenvolvimento deste sistema, damos preferência à utilização de ferramentas, bibliotecas e \textit{frameworks} de \emph{Software} Livre já existentes.
	\item \textbf{Padrões abertos:} utilizar padrões bem definidos e amplamente usados tanto na academia quanto na indústria, facilita a integração de novos desenvolvedores bem como a utilização do sistema por \emph{software} cliente. Desta forma, o uso de padrões é sempre recomendado, priorizando padrões abertos.
	\item \textbf{Modularidade do projeto:} buscando facilitar o ingresso de novos desenvolvedores, o projeto é modularizado de modo que modificações locais causem apenas impactos locais, mantendo a complexidade baixa mesmo com a evolução da aplicação.
	\item \textbf{Modelo de dados não-relacional:} para atingir o objetivo de criar uma plataforma capaz de abstrair o formato dos dados recebidos e oferecer às aplicações dados no mesmo formato, o modelo dos dados deve ser capaz de se adaptar as diferenças encontradas e, com isso, optamos por modelos não-relacionais de dados.
\end{itemize}

\section{Arquitetura}

A arquitetura proposta está descrita na Figura \ref{fig:arch}, apresentando três principais componentes: as aplicações, o \textit{serviço} \footnote{\url{https://gitlab.com/interscity/health-dashboard/datahealth-api}}, e as bases de dados. Considerando o nível de desacoplamento proposto, os componentes podem ser desenvolvidos e mantidos por desenvolvedores diferentes, comunicando-se via chamadas \textit{REST}.

\begin{figure}
	\begin{center}
		\includegraphics[width=.7\textwidth]{Arquitetura}
	\end{center}
	\captionof{figure}{Arquitetura Proposta}
	\label{fig:arch}
\end{figure}

As aplicações fazem referência as diferentes formas de visualização que serão utilizadas a partir dos dados disponibilizados. O \textit{serviço} é composto de quatro módulos (\textit{Consulta}, \textit{Agrupamento}, \textit{Geoanonimização} e \textit{Registro}) que atuam em conjunto para receber dados geolocalizados, processá-los e disponibilizá-los a aplicações por meio de requisições \textit{REST}. 

Entre as aplicações, destacamos o \emph{GeoMonitor da Saúde} (vide Capítulo \ref{cap:geomonitor}), desenvolvido para visualizar dados georreferenciados referentes aos sistemas de informação de mortalidade (SIM), de nascidos vivos (SINASC), e internações hospitalares (SIH) fornecidos pela SMS-SP. O desafio de criar uma mesma visualização para diferentes bases de dados foi a motivação para a elaboração da arquitetura descrita a seguir.

\subsection{Aplicações}

Nesta camada está a interface com os usuários. A flexibilidade fornecida por esta arquitetura torna possível que o desenvolvedor desta camada possa criar visualizações complexas para dados georreferenciados e, utilizando a \textit{API}, utilizar como fonte dados variados sem a necessidade de fazer grandes modificações em sua aplicação. A comunicação entre aplicações e o \textit{serviço} é feita via chamadas \textit{REST}, onde parâmetros são enviados em formato \textit{JSON} e as respostas do \textit{serviço} são em \textit{GEOJSON}.

\subsection{Serviço}

O \textit{serviço}, principal componente do projeto, faz a comunicação entre os dados e as aplicações que buscam fazer análises sobre eles. Para isso, os dados recebidos são tratados e armazenados de modo que a aplicação cliente recebe, ao fazer uma requisição, dados formatados em \textit{GEOJSON}. Com isso diferentes bases de dados podem ser utilizados pela mesma aplicação de visualização.

Os módulos internos do \textit{serviço} estão divididos em dois grupos:

\begin{itemize}
	\item \textbf{Tratamento dos dados:} os módulos \textbf{\textit{Registro}} (vide Seção \ref{sec:registro}) e \textbf{\textit{Geoanonimização}} (vide Seção \ref{sec:anom}) tratam os dados recebidos a partir dos \textit{importadores de dados}. São responsáveis por checar se o formato está correto, bem como verificar se os dados são geolocalizadas, além disso outro papel fundamental é o de garantir que não seja possível identificar o local de origem dos dados \footnote{O processo de geolocalização possuí grande precisão, tornando possível identificar o endereço original, o módulo de \emph{Geoanonimização} tem a função de aplicar um deslocamento impossibilitando assim a identificação do paciente.}. 
	\item \textbf{Serviço a aplicações:} os módulos \textbf{\textit{Consulta}} (vide Seção \ref{sec:consulta}) e \textbf{\textit{Agrupamento}} (vide Seção \ref{mod:agrupamento}) lidam com as requisições de aplicações clientes que buscam os dados disponíveis no serviço. Eles são responsáveis pelo processamento desses dados com o objetivo de garantir o mesmo formato de resposta para a aplicação que fez a requisição.
\end{itemize}

A seguir estão descritos os módulos do \textit{serviço}.

\subsubsection{Registro}
\label{sec:registro}
Com a intenção de criar um \textit{serviço} genérico para atender a diversas bases de dados, o módulo de Registro foi projetado para fornecer um ponto de entrada de novos registros neste \textit{serviço}. Um registro é um dado georreferenciado que é armazenado pelo serviço para ser disponibilizado às aplicações. A Figura \ref{geojson1} ilustra a estrutura de um registro em que é necessário definir o tipo, coordenadas e suas propriedades. No \emph{GeoMonitor da Saúde} (vide Capítulo \ref{cap:geomonitor}) uma internação hospitalar é um registro.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"type": "Feature",
		"geometry": {
			"type": "Point",
			"coordinates": [long, lat]	
		},
		"properties": {
			"prorpiedade_1": "valor_propriedade_1",
			"propriedade_2": "valor_propriedade_2",
			"propriedade_n": "valor_propriedade_n"	
		}
	}
	\end{lstlisting}
	\caption{Exemplo de um registro}
	\label{geojson1}
\end{figure}

Este módulo proporciona uma interface para consultar, alterar e remover registros existentes. Podendo ser utilizado diretamente por meio de requisições \textit{REST} ou por meio de aplicações facilitadoras aqui denominadas \textit{importadores de dados} (vide Seção \ref{sec:importadores}), esta utilização é feita nos seguintes \textit{endpoints}:

\bigskip
\begin{itemize}
	\item \textbf{\textit{POST /record}}
	
	Salva o registro.

	\item \textbf{\textit{GET /record/:\_id}}

	Retorna as informações do registro que possui o id passado como parâmetro.

	\item \textbf{\textit{PUT /record/:\_id}}

	Altera valores de um registro de acordo com os valores passados na requisição. 

	\item \textbf{\textit{DELETE /record/:\_id}}

	Remove o registro que possui o valor id.
\end{itemize}
\bigskip

Para tornar possível o registro de base de dados diferentes com campos distintos, utilizaremos internamente um banco de dados \textit{NoSQL} orientado a documentos. Nesse caso, no formato \textit{GEOJSON}. Com isso \textbf{\textit{POST /record}} recebe documentos no formato descrito na Figura \ref{geojson1}.

\begin{itemize}
	\item \textbf{type:} indica para o \textit{parser} interno se o documento contém informações de um ponto ``Feature'' ou de um conjunto de pontos ``FeatureCollection''.
	\item \textbf{\textit{geometry:}} neste campo estão contidas informações da geolocalização do registro.
	\begin{itemize}
		\item \textbf{\textit{type:}} o formato \textit{GEOJSON} é capaz de identificar sete formatos geométricos. Os dados utilizados são do formato \textit{Point}, ou seja são pontos geométricos.
		\item \textbf{\textit{coordinates:}} identifica as coordenadas \footnote{Coordenadas no sistema WGS 84} do ponto registrado.
	\end{itemize}
	\item \textbf{\textit{properties:}} todas informações referentes ao registro ficaram armazenadas neste campo.
\end{itemize} 
\bigskip

O campo \textbf{\textit{properties}} é o responsável por atingir o nível de abstração dos dados desejado. Se a base SIH possui os campos (ESPECIALIDADE, CNES, COMPLEXIDADE) e a base SIM (DIARIAS, P\_IDADE, CAR\_INTERNAÇÃO), é possível salvar os dados de ambas com a mesma chamada alterando apenas os campos do objeto enviado em \emph{properties}. O banco de dados \textit{NoSQL} nos dá a capacidade de fazer consultas utilizando as propriedades, apesar dos valores serem dinâmicos.

Entre as informações enviadas em \textbf{\textit{properties}} damos destaque à propriedade \textbf{\textit{database}} que identifica a base de dados a que o registro pertence, facilitando o consumo desses dados por aplicações que utilizarão o serviço como fonte de dados. Portanto, é essencial que todos os registros sejam identificados quanto a sua origem.

A aplicação é capaz de receber múltiplos registros em uma única chamada. Com isso, a quantidade de chamadas efetuadas ao inserir os dados é reduzida. Isso é alcançado utilizando o \emph{type} ``FeatureCollection'' do \textit{GEOJSON}, conforme exemplificado na Figura \ref{geojson2}.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"type": "FeatureCollection",
		"features": [
			{
				"type": "Feature",
				"geometry": {
					"type": "Point",
					"coordinates": [long1, lat1]			
				},
				"properties": {
					"propriedade1": "valor_propriedade1",			
					"propriedade2": "valor_propriedade2",
					...
				}
			},
			{
				"type": "Feature",
				"geometry": {
					"type": "Point",
					"coordinates": [long2, lat2]			
				},
				"properties": {
					"propriedade1": "valor_propriedade1",			
					"propriedade2": "valor_propriedade2",
					...
				}
			}
		]
	}
	\end{lstlisting}
	\caption{Exemplo de \textit{GEOJSON} com múltiplos pontos}
	\label{geojson2}
\end{figure}

Após validar o documento recebido e verificar que ele possui o formato correto, o módulo de \textit{Geoanonimização} (vide Seção \ref{sec:anom}) é acionado antes de concluir o registro do dado. 

Esse formato de registro de dados, portanto, estende o escopo dos dados que podem ser visualizados nas aplicações. De forma geral, qualquer dado geolocalizado pode ser adicionado à aplicação, possibilitando que uma mesma aplicação de visualização seja utilizada para dados de fontes diferentes.

\subsubsection{Geoanonimização}
\label{sec:anom}

Parte fundamental da utilização de dados da Saúde é impossibilitar a identificação do paciente por terceiros. Atualmente técnicos da SMS-SP tem o papel de fazer a anonimização dos dados, ela é feita deslocando o local de origem do paciente para o centróide do setor censitário \footnote{Setor censitário é a unidade territorial de controle cadastral da coleta do censo do IBGE, constituída por áreas contíguas, respeitando-se os limites da divisão político-administrativa, dos quadros urbano e rural legal e de outras estruturas territoriais de interesse, além dos parâmetros de dimensão mais adequados à operação de coleta.} em que ele reside. O processo atual é demorado e custoso para a SMS-SP. Desta forma, o serviço proposto oferece um módulo de \textit{Geoanonimização} que tem por objetivo fazer todo o processo de anonimização dos dados de forma automática assim que os dados são recebidos. O módulo proposto recebe uma coordenada geográfica e retorna o centróide do setor censitário em que ela se encontra.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=.7\linewidth]{Setores_mapshapper}
	\end{center}
	\captionof{figure}{Setores Censitários da cidade São Paulo \protect\footnotemark}
\end{figure}

\footnotetext{\url{https://mapshaper.org/}}

Para efetuar essa tarefa, utilizamos dados do Censo Demográfico de 2010 que fornece os polígonos que identificam os setores censitários de todo o Brasil. Separamos os dados referentes à cidade de São Paulo. Esses arquivos foram convertidos para o formato \textit{GEOJSON} possibilitando a criação de uma base de dados georreferenciada que identifica os setores censitários como objetos geométricos. Nessa base, é possível fazer consultas georreferenciadas. Com a coordenada recebida no modulo de \textit{registro}, é feita uma consulta que resulta no setor censitário em que a coordenada se encontra. Na nossa base de dados, a coordenada salva é o centróide do polígono resultado desta consulta, ou seja, o centróide do setor censitário garantindo, assim, a anonimidade do dado. É importante mencionar que o Censo Demográfico possui dados de todo Brasil, assim esse processo pode ser efetuado para qualquer cidade do Brasil.

\subsubsection{Consulta}
\label{sec:consulta}

O módulo de \textit{consulta} foi projetado para entregar às aplicações dados com todas as suas informações, sem nenhum tipo de tratamento, além da aplicação dos filtros passados na requisição. Ele surge da necessidade que certas aplicações possuem de apresentar informações completas sobre os dados, ou se a aplicação deseja fazer uma análise dos dados que o formato oferecido pelo módulo de \textit{Agrupamento} (vide Seção \ref{mod:agrupamento}) não é capaz de atender. A requisição é feita no \textit{endpoint} \textit{/query}, descrito abaixo:

\bigskip
\begin{itemize}
	\item \textbf{\textit{POST /query}} este \textit{endpoint} pode ser usado para buscar registros que correspondem aos filtros especificados na chamada.
	Os filtros podem ser da seguinte forma:
	\bigskip
	\begin{itemize}
		\item \textbf{\textit{properties:}} filtra os dados de acordo com as propriedades. Este filtro será usado para criar consultas dinâmicas no banco de dados, portanto os valores passados como parâmetros devem coincidir com valores que os registros salvos possuem. Os seguintes operadores são aceitos:
	\bigskip
	\begin{itemize}
		\item \textbf{eq} - o registro deve ter valor igual ao parâmetro.
		\item \textbf{gt} - o registro deve ter valor maior que o parâmetro.
		\item \textbf{gte} - o registro deve ter valor maior que ou igual ao parâmetro.
		\item \textbf{lt} - o registro deve ter valor menor que o parâmetro.
		\item \textbf{lte} - o registro deve ter valor menor que ou igual ao parâmetro
		\item \textbf{in} - o valor deve estar contido na lista de valores especificada na chamada.
		\item \textbf{ne} -  o registro deve ter valor diferente do parâmetro.
	\end{itemize}
	\bigskip
	A requisição terá o formato visto na Figura: \ref{json1}

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"properties": {
			"database": "SIM",
			"CNES": ["eq", "201030"],
			"P_IDADE": ["gt", 26]	
		}
	}
	\end{lstlisting}
	\caption{Requisição aceita pelo \textit{endpoint} \textit{/group}}
	\label{json1}
\end{figure}

	\item \textbf{\textit{location:}} para receber registros de acordo com  uma localização, informando os parâmetros lat e long.  Como resposta, teremos os registros que estão no ponto específico ou, enviando também um parâmetro \textit{radius}, os registros num raio de distância do ponto informado, como mostrado na Figura \ref{json2}.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"properties": {
			"database": "SIH",
			"location": [long, lat],
			"radius": 6.5	
		}
	}
	\end{lstlisting}
	\caption{Dados da requisição aceita pelo \textit{endpoint} \textit{/query} geolocalizado}
	\label{json2}
\end{figure}

\end{itemize}
\end{itemize}

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"type": "FeatureCollection",
		"features": [
			{
				"type": "Feature",
				"geometry": {"type": "Point", "coordinates": [long1, lat1]},
				"properties": {"database": "SIH", "propriedade1": "valor_propriedade1", "propriedade2": "valor_propriedade2" ...}
			},
			{
				"type": "Feature",
				"geometry": {"type": "Point", "coordinates": [long2, lat2]},
				"properties": {"database": "SIM", "propriedade1": "valor_propriedade1", "propriedade2": "valor_propriedade2" ...}
			},
			{
				"type": "Feature",
				"geometry": {"type": "Point", "coordinates": [long3, lat3]},
				"properties": {"database": "SINASC", "propriedade1": "valor_propriedade1", "propriedade2": "valor_propriedade2" ...}
			}
		]
	}
	\end{lstlisting}
	\caption{Resposta do \textit{endpoint} \textit{/query}}
	\label{geojson3}
\end{figure}

A resposta dessa requisição será um arquivo \textit{GEOJSON} com os registros que se enquadram nos filtros enviados, como ilustrado na Figura \ref{geojson3}.

\subsubsection{Agrupamento}
\label{mod:agrupamento}

O módulo de \textit{agrupamento} foi desenvolvido com o objetivo de auxiliar aplicações que farão análises dos dados registrados na aplicação. Ele alcança esse objetivo fornecendo ao cliente uma série de operadores capazes de agrupar os dados de várias formas em velocidade superior a que as aplicações seriam capazes.

O módulo de \textit{agrupamento} faz parte, juntamente com o módulo de \textit{consulta}, da integração com as aplicações que utilizam os dados. O \textit{endpoint} a ser utilizado é o \textit{/group}, descrito abaixo:

\bigskip
\begin{itemize}
	\item \textbf{\textit{POST} \textit{/group}} da mesma forma que \textit{/query} este \textit{endpoint} recebe um \textit{JSON} com filtros. Adicionalmente, o usuário pode enviar um campo pelo qual os dados serão agrupados e um operador de \textit{agrupamento}. Os seguintes operadores são aceitos:
	\bigskip
	\begin{itemize}
		\item \textbf{avg:} a média dos valores do campo especificado.
		\item \textbf{max:} o maior valor para o campo especificado.
		\item \textbf{min:} o menor valor para o campo especificado.
		\item \textbf{sum:} a soma dos valores do campo especificado.
		\item \textbf{count:} a quantidade de registros por valor distinto do campo especificado.
	\end{itemize}
	\bigskip

O \textit{endpoint} \textit{/group} aceita como entrada arquivos JSON com o formato visto na Figura \ref{json3}.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"properties": {
				"database": "SIH",
				"CNES": ["eq", 291245]
		},
		"group": ["count", ["coordinates"]]
	}
	\end{lstlisting}
	\caption{Dados da requisição aceita pelo \textit{endpoint} \textit{/group}}
	\label{json3}
\end{figure}

Assim como \textit{/query} a resposta deste \textit{endpoint} é um arquivo \textit{GEOJSON} no formato apresentado na Figura: \ref{geojson4}

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"type": "FeatureCollection",
		"features": [
			{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lat1, long1], "properties": {"counter": 10}}},
			{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lat2, long2], "properties": {"counter": 5}}},
			{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lat3, long3], "properties": {"counter": 20}}},
			{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lat4, long4], "properties": {"counter": 2}}},
			{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lat5, long5], "properties": {"counter": 1}}}
		]
	}
	\end{lstlisting}
	\caption{Resposta do \textit{endpoint} \textit{/group}}
	\label{geojson4}
\end{figure}

Note que, nesse caso, cada ponto possui, em propriedades, o valor de \textit{counter}. Naquele ponto foram encontrados \textit{counter} registros. Esse \textit{agrupamento} por coordenadas auxilia, por exemplo, na clusterização efetuada na aplicação GeoMonitor da Saúde (vide Capítulo \ref{cap:geomonitor}) diminuindo o número total de pontos.
\end{itemize}

\subsection{Importador de dados}
\label{sec:importadores}

A SMS-SP recebe os dados em formatos variados (\textit{CSV}, \textit{Shapefile}, \textit{xls}, etc.). No entanto, o serviço proposto,  em seu \textit{endpoint} de registro, recebe os dados no formato \textit{GEOJSON}. Para facilitar o processo de registro dos dados, uma pequena aplicação foi criada para converter os dados para o formato adequado. Com isso, o trabalho de inserção dos dados é automatizado, facilitando o trabalho de quem atualiza a aplicação e evitando o registro de dados incorretos.

O \textit{importador de dados} proposto recebe dados de duas formas distintas:

\begin{itemize}
	\item \textbf{Arquivo:} o \textit{importador} será capaz de ler um arquivo do tipo \textit{CSV} (vide Figura \ref{fig:csv_exemple}) ou \textit{Shapefile}, e gerar automaticamente documentos \textit{GEOJSON} e registrá-los na aplicação.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=.7\textwidth]{csv_exemple}
	\end{center}
	\captionof{figure}{Exemplo de Arquivo \textit{CSV} aceito pelo \textit{importador de dados}}
	\label{fig:csv_exemple}
\end{figure}

	\item \textbf{Input do usuário:} a aplicação terá uma página onde o usuário poderá descrever o registro a ser adicionado. Com esses dados o \textit{importador} fará o processo de registro na aplicação.
\end{itemize}

O \textit{importador de dados} também deve ser capaz de geolocalizar registros. Um requisito para o desenvolvimento da arquitetura é integrar o processo de \textit{geoanonimização} à aplicação. Parte desse processo envolve receber um endereço (CEP, logradouro, nome etc.) e encontrar as coordenadas (longitude, latitude) desse endereço. Essa função é desempenhada pelo \textit{importador}, pois o documento no formato \textit{GEOJSON} deve conter coordenadas, que serão tratadas pelo módulo de \textit{Geoanonimização}. Com isso, o usuário pode cadastrar no \textit{importador} um registro no formato apresentado na Figura \ref{json4}.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"ENDERECO": "Rua do Matao, 1010",
		"properties": {
			"ESPECIALID": 1, "CMPT": 201508, "DT_EMISSAO": "2015-06-22"	
		}
	}
	\end{lstlisting}
	\caption{Entrada do \textit{Importador de dados}}
	\label{json4}
\end{figure}

O \textit{importador}, portanto, criará um arquivo \textit{GEOJSON} no formato ilustrado na Figura \ref{geojson5}.

\begin{figure}
	\begin{lstlisting}[language=java, style=wider]
	{
		"type": "Feature,
	  	"geometry": {
	  		"type": "Point",
	  		"coordinates": [-46.7319998, -23.559667],
	  		"properties": {
	  			"ESPECIALIDADE": 1, "CMPT": 201508, "DT_EMISSAO": "2015-06-22"
	  		}
	  	}
	}
	\end{lstlisting}
	\caption{Saída do \textit{importador de dados}}
	\label{geojson5}
\end{figure}

Dado o arquivo descrito na Figura \ref{fig:csv_exemple}, o \textit{script} descrito na Figura \ref{ruby1} atua como um \textit{importador de dados}. O \textit{script} lê cada linha do arquivo, com o valor do CEP encontrado, utiliza a biblioteca \textit{geocoder} \footnote{\url{http://www.rubygeocoder.com/}} para encontrar as coordenadas daquele local. Depois do processo de geolocalização, os dados são formatados em \textit{GEOJSON} e enviados para a aplicação com uma requisição \textit{POST} no \textit{endpoint} \textit{/record}.

\begin{figure}
	\begin{lstlisting}[language=ruby, style=wider]
	require 'json'
	require 'geocoder'
	require 'csv'
	require 'rest-client'

	CSV.foreach(procedure_csv_path, :headers => true) do |row|
		cep = row[4]
	    location = Geocoder.search(cep).first

	    values = {}
	    values[:properties] = {}
	    values[:properties][:database] = "SIH"
	    values[:properties][:CD_GEOCODI] = row[0]
	    values[:properties][:ESPECIALID] = row[1]
	    values[:properties][:CMPT] = row[2]
	    values[:properties][:DT_EMISSAO] = row[3]

	    point = {}
	    point[:type] = "Point"
	    point[:coordinates] = [location[0].to_f, location[2].to_f]

		RestClient::Request.execute(method: :post,
	                            url: ENV["API_URL"],
	                            payload: values.to_json,
	                            headers: {"Content-Type" => "application/json"}
	                           )
	end
	\end{lstlisting}
	\caption{\textit{Script} para registrar dados na aplicação}
	\label{ruby1}
\end{figure}

\section{Detalhes de Implementação}

Esta seção apresenta escolhas técnicas feitas durante o desenvolvimento da aplicação.

\subsection{\textit{RESTFul API}}

Este padrão de arquitetura foi adotado dada sua grande aceitação por parte da comunidade de desenvolvedores tornando assim, a aplicação mais atrativa a potenciais desenvolvedores que buscam \textit{APIs} abertas para serem base de seus aplicativos. Além disso, facilita o acoplamento de aplicações já existentes que utilizam esse padrão. 

\subsection{\textit{Ruby On Rails} \protect\footnotemark}

\footnotetext{\url{https://rubyonrails.org/}}
É um \textit{framework} para desenvolvimento de aplicações \textit{Web} e \textit{serviços}. Foi escolhido dada a familiaridade do grupo de sistemas do IME-USP de desenvolvimento com a linguagem \textit{Ruby}, além dos seguintes fatores:

\begin{itemize}
	\item Software Livre com grande comunidade ativa de desenvolvedores.
	\item Modo \textit{API-Only}\footnote{\url{https://guides.rubyonrails.org/api_app.html}} que facilita o processo de criação de um serviço.
	\item Grande quantidade de \textit{gems} \footnote{\textit{Gem} é a denominação de um programa ou biblioteca \textit{ruby} disponibilizado para uso pela comunidade de desenvolvedores.} tornando o reuso de código fácil e intuitivo.
\end{itemize}

\subsection{\textit{MongoDB} \protect\footnotemark}

\footnotetext{\url{https://www.mongodb.com/}}
Banco de dados orientado a documentos permite maior flexibilidade quanto aos dados que cada registro deve possuir, sem perder a capacidade de manipulação dos dados (consultas, agrupamentos, contagem etc). Assim, o sistema de gerenciamento de banco de dados escolhido foi o \textit{MongoDB}, pelos motivos elencados a seguir:

\begin{itemize}
	\item Software Livre com comunidade de desenvolvedores grande e em expansão.
	\item Facilidade de integração com \textit{Ruby on Rails}.
	\item Módulo para dados geolocalizados que melhora o desempenho da aplicação e fornece consultas geolocalizadas.
\end{itemize}

\subsection{\textit{GEOJSON} \protect\footnotemark}

\footnotetext{\url{http://geojson.org/}}
O padrão \textit{GEOJSON} foi adotado por sua facilidade de lidar com objetos geolocalizados. A aplicação receberá dados majoritariamente desta forma. Além disso, com este padrão podemos interagir com diversos sistemas já existentes que fazem sua comunicação por este formato. Facilitando o desenvolvimento de aplicações no \textit{front-end}, e possibilitando que aplicações já existentes passem a utilizar a \textit{API} disponibilizada.

\section{Utilização do \textit{serviço} para base de dados geolocalizados da saúde}

Nesta seção, apresentamos o fluxo dos dados entre os diferentes componentes da arquitetura proposta. Usaremos como exemplo a aplicação \textit{GeoMonitor da Saúde} utilizando a base de dados SIH.

\begin{figure}[ht]
	\centering
	\begin{minipage}[b]{0.33\textwidth}
		\includegraphics[width=\textwidth]{Fluxo1}
		\caption{Fluxo de registro de dados na aplicação}
		\label{fig:fluxo1}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{Fluxo2}
		\caption{Fluxo de consulta de dados na aplicação}
		\label{fig:fluxo2}
	\end{minipage}
\end{figure}

\pagebreak

A Figura \ref{fig:fluxo1} apresenta a entrada de dados no serviço. Os dados referentes ao SIH estão armazenados no formato \textit{CSV}, eles são lidos pelo \textit{Importador de dados} que cria arquivos \textit{GEOJSON}, e forma requisições para o \textit{endpoint /record}. Nesse ponto, os dados entram no serviço, aqui o módulo de \textit{Geoanonimização} é utilizado para anonimizar os dados que então são salvos na base de dados.

Quando uma aplicação faz uma requisição por dados nos \textit{endpoints} \textit{/query} ou \textit{/group}, temos o fluxo de dados como ilustrado na Figura \ref{fig:fluxo2}. A requisição é validada quanto a seu formato; a \textit{query} é formulada pelo módulo que foi acionado e executada pela instância do banco de dados; o resultado é então formatado em \textit{GEOJSON} e enviado na resposta da requisição.
