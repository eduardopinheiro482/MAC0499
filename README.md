# MAC0499 Trabalho de Formatura Supervisionado

## Um serviço para o desenvolvimento de aplicações de visualização de dados de saúde georreferenciados

As tecnologias de Cidades Inteligentes emergem como uma solução para enfrentar problemas comuns em grandes centros urbanos, usando os recursos da cidade de forma mais eficiente e proporcionando serviços de melhor qualidade para os cidadãos. Investigar estratégias e desenvolver aplicações para visualização espacial de grandes quantidades de dados de cidades, como São Paulo, a partir de múltiplas fontes heterogêneas se faz cada vez mais necessário na área da Computação. Nesta monografia, como estudo de caso para o desenvolvimento de uma aplicação de clusterização de dados, trabalhamos com dados públicos do Sistema Único de Saúde (SUS), fornecidos pela Secretaria Municipal de Saúde de São Paulo (SMS-SP). A grande quantidade de dados heterogêneos sobre saúde nas grandes cidades torna necessário a criação de novas formas de visualização interativa e georreferenciada.  Nesse sentido, há diversas oportunidades de colaborações inerentes ao desenvolvimento dessas soluções, em especial para que pesquisadores da área da Saúde e da Ciência da Computação possam contribuir com formas inovadoras para coleta, armazenamento, gestão, visualização e análise de grandes quantidades de dados de saúde de populações urbanas. Esta monografia propõe uma arquitetura de referência para armazenamento de dados georreferenciados e apresenta uma aplicação piloto a ser utilizada pela SMS-SP.

---
### Aluno: Eduardo Pinheiro
### Orientador: Paulo Meirelles
### Coorientador: Fabio Kon